<?php

include_once "config.php";

spl_autoload_register(function ($class) {
    $class_path = __DIR__ . "/classes/" . $class . ".class.php";
    if (file_exists($class_path)) {
        include_once $class_path;
    } else {
        echo "Class ", $class, " not found";
        exit(1);
    }
});