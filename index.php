<?php

error_reporting(E_ALL);

include_once __DIR__ . "/autoload.php";

$db = new DB($host, $user, $pass, $dbname);
$template = new Template($template_dir);

if (count($_POST) > 0) {
    $validate = new Validate($_POST);
    $validate_result = $validate->validate();
    echo json_encode($validate_result);
    if ($validate_result['result']) {
        $feedback = new Feedback($db);
        $feedback->save($_POST);
    }
    die();
}

$data["title"] = "Test task";
$template->render("index", $data);



