<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/css/custom.css"/>
</head>
<body>

<header></header>

<main>
    <div class="form">

        <form name="feedback" id="feedback">

            <div class="header">
                <span>Написать письмо</span>
            </div>

            <div class="row">
                <input type="text" name="name" placeholder="Имя">
            </div>

            <div class="row">
                <input type="email" name="email" placeholder="E-mail">
            </div>

            <div class="row">
                <textarea name="message"></textarea>
            </div>

            <div class="row errors_area">
                * Все поля обязательны для заполнения
            </div>

            <div class="row">
                <button type="button" name="submit">Отправить</button>
            </div>

        </form>


    </div>
</main>

<foooter>
</foooter>

</body>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/js/custom.js"></script>

</html>