<?php

/**
 *
 * Class Validate
 *
 * @property $fields
 *
 */
class Validate
{
    const NAME_FIELD = "name";
    const EMAIL_FIELD = "email";
    const MESSAGE_FIELD = "message";

    public $fields;
    private $errors = [];

    /**
     *
     * Validate constructor.
     * @param array $fields
     *
     */
    public function __construct($fields)
    {
        $this->fields = $fields;
    }

    /**
     *
     * Validate all fields
     *
     * @return array
     */
    public function validate()
    {
        foreach ($this->fields as $field => $value) {
            $this->validate_field($field, $value);
        }

        if (count($this->errors) > 0) {
            return ['result' => false, 'errors' => $this->errors];
        }

        return ['result' => true, 'errors' => []];
    }

    /**
     *
     * Validate each field
     *
     * @param string $field
     * @param string $value
     */
    private function validate_field(string $field = "", string $value = "")
    {
        if ($field == self::NAME_FIELD) {
            if (!preg_match('/[A-Za-z0-9]{4,}/', $value)) {
                $this->errors[self::NAME_FIELD] = "Поле Имя заполнено некорректно. Поле должно содержать латинские буквы и цифры и состоять минимум из 4 символов";
            }
        }

        if ($field == self::EMAIL_FIELD) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->errors[self::EMAIL_FIELD] = "Поле Email заполнено некорректно";
            }
        }

        if ($field == self::MESSAGE_FIELD) {
            if (mb_strlen($value) < 10 || trim($value) == '') {
                $this->errors[self::MESSAGE_FIELD] = "Поле Сообщение заполнено некорректно. Поле должно состоять минимум из 10 символов";
            }
        }
    }

}