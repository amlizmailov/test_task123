<?php

/***
 *
 * Class Feedback
 *
 * @property $db
 */
class Feedback
{
    public $db;

    /**
     * Feedback constructor.
     */
    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'feedback';
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function save($attributes = [])
    {
        if (count($attributes) == 0) {
            return false;;
        }

        $attributes['ip'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        $attributes['create_time'] = time();

        $fields = array_keys($attributes);
        $values = [];
        foreach ($attributes as $field => $value) {
            $values[] = "'" . $value . "'";
        }

        $sql = "INSERT INTO " . $this->getTableName() . "(" . implode(",", $fields) . ") VALUES(" . implode(",", $values) . ")";

        return $this->db->query($sql);

    }
}