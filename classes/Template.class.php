<?php

/**
 *
 * Class Template
 *
 * @property $template_dir
 *
 */

class Template
{
    public $template_dir;

    /**
     * Template constructor.
     * @param string $template_dir
     */
    public function __construct(string $template_dir)
    {
        $this->template_dir = $template_dir;
    }

    /**
     * @param string $template
     * @param array $data
     */
    public function render(string $template, $data = [])
    {
        extract($data);

        $tmpl_suffix = ".php";
        $template = $this->template_dir . $template . $tmpl_suffix;

        if (!file_exists($template)) {
            throw new Exception("Template $template not found");
        }

        include_once $template;
    }
}