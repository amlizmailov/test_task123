<?php

/**
 *
 * Class DB
 *
 * @property $host
 * @property $user
 * @property $pass
 * @property $dbname
 *
 */
class DB
{
    private $link;
    public $host;
    public $user;
    public $pass;
    public $dbname;

    /**
     * DB constructor.
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $dbname
     */
    public function __construct(string $host, string $user, string $pass = "", string $dbname)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;

        $this->connect();
    }

    /**
     * @return void
     */
    private function connect()
    {
        $this->link = mysqli_connect($this->host, $this->user, $this->pass, $this->dbname);
    }

    /**
     * @param string $query
     * @return bool|mysqli_result
     */
    public function query(string $query)
    {
        return mysqli_query($this->link, $query);
    }

    /**
     * @param mysqli_result $result
     * @return array|null
     */
    public function fetch_array(mysqli_result $result)
    {
        return mysqli_fetch_array($result);
    }

    /**
     * @param mysqli_result $result
     * @return array|null
     */
    public function fetch_row(mysqli_result $result)
    {
        return mysqli_fetch_row($result);
    }
}