$(document).ready(function () {

    $(document).on('click', '#feedback button', function (e) {
        e.preventDefault();

        var form = $('#feedback');

        form.find('input, textarea').removeClass('error').removeClass('success');

        form.find('input, textarea').addClass('success');

        $.ajax({
            url: '/index.php',
            method: 'POST',
            dataType: 'JSON',
            data: $(form).serialize(),
            success: function (resp) {

                if (resp.result == true) {

                    form.find('input, textarea');

                    form.find('.errors_area').html('<span class="success">Ваше сообщение принято!</span>');

                }
                else {

                    err_msg = '';

                    $.each(resp.errors, function (index, value) {

                        $('input[name="' + index + '"]').removeClass('success').addClass('error');
                        $('textarea[name="' + index + '"]').removeClass('success').addClass('error');

                        err_msg += value + '<br>';
                    })

                    form.find('.errors_area').html(err_msg);
                }
            }
        });
    });

});